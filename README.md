# A study on travel time and the underlying physical model of Quantum Drives in Star Citizen

Investigation of the underlying physical behaviour of Quantum Drives and development of a mathematical model to predict travel time.

# 7 April 2021

Publication of initial version

# 25 September 2021

Corrigendum (1)

# 15 October 2021

Addendum (1) Non-complex calculation variant

# 9 February 2022

Integration of the Corrigendum and Addendum into the document structure. Corrections in figure 3 and 6. Renaming of variable as to ma. More precise definition of error assumption in the conclusion. Addition official disclaimer to the document.